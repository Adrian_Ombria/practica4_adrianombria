package clases;

import java.time.LocalDate;

public class Personaje {

	private String nombre;
	private int nivel;
	private String clase;
	private LocalDate fechaCreacion;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getNivel() {
		return nivel;
	}
	public void setNivel(int nivel) {
		this.nivel = nivel;
	}
	public String getClase() {
		return clase;
	}
	public void setClase(String clase) {
		this.clase = clase;
	}
	public LocalDate getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(LocalDate fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	public Personaje(String nombre) {
		this.nombre = nombre;
	}
	
	public String toString() {
		return nombre + " " + nivel + " " + clase + " " + fechaCreacion;
	}
	
}

