package clases;

import java.time.LocalDate;

public class Hermandad {

private Personaje[] personajes;
	
	public Hermandad(int numMaxPersonajes) {
		this.personajes = new Personaje[numMaxPersonajes];
	}
	
	public void altaPersonaje(String nombre, int nivel, String clase) {
		for(int i = 0; i < personajes.length; i++) {
			if(personajes[i] == null) {
				personajes[i] = new Personaje(nombre);
				personajes[i].setNivel(nivel);
				personajes[i].setClase(clase);
				personajes[i].setFechaCreacion(LocalDate.now());
			break;
			}
		}
	}
	
	public Personaje buscarPersonaje(String nombre) {
		for(int i = 0; i < personajes.length; i++) {
			if(personajes[i] != null) {
				if(personajes[i].getNombre().equals(nombre)) {
					return personajes[i];
				}
			}
		}
		
		return null;
	}
	
	public void eliminarPersonaje(String nombre) {
		for(int i = 0; i < personajes.length; i++) {
			if(personajes[i] != null) {
				if(personajes[i].getNombre().equals(nombre)) {
					personajes[i] = null;
				}
			}
		}
	}
	
	public void listarPersonajes() {
		for(int i = 0; i < personajes.length; i++) {
			if(personajes[i] != null) {
				System.out.println(personajes[i]);
			}
		}
	}
	
	public void cambiarPersonaje(String nombre, String clase2) {
		for(int i = 0; i < personajes.length; i++) {
			if(personajes[i] != null) {
				if(personajes[i].getNombre().equals(nombre)) {
				   personajes[i].setClase(clase2);
				}
			}
		}
	}
	
	public void listarPersonajesPorClase(String clase) {
		for(int i = 0; i < personajes.length; i++) {
			if(personajes[i] != null) {
				if(personajes[i].getClase().equals(clase)) {
					System.out.println(personajes[i]);
				}
			}
		}
	}
}

